package customers.microservice.controllers;

//import org.apache.commons.lang3.StringEscapeUtils;

import org.apache.commons.text.StringEscapeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController("/customers")
public class CustomersController {

    private static Logger logger = LoggerFactory.getLogger(CustomersController.class);

    @GetMapping
    public String getName(@RequestParam(name = "p1") String p1){
        logger.debug(StringEscapeUtils.unescapeJava(p1));
        return "hi!";
    }

}
