package customers.microservice;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;



@Service
public class CustomerServiceImpl implements CustomerService {
    //private int x;

    private static Logger logger = LoggerFactory.getLogger(CustomerServiceImpl.class);

    @Override
    public int sumar(int n1, int n2) {

        return 0;

    }

    @Override
    public void doProcess(String message) {
        logger.debug(message);
    }
}
